<?php

use App\Http\Controllers\Backoffice\Authorization\AuthorizationController;
use App\Http\Controllers\Backoffice\Links\LinkController;
use App\Http\Controllers\Backoffice\Tracking\DashboardController;
use App\Http\Controllers\Backoffice\Tracking\TrackingController;
use App\Http\Controllers\Backoffice\Users\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//////
//
//Backoffice routes group
//
//////
Route::group(['prefix'=>'backoffice'],static function(){
    Route::post('/login',[AuthorizationController::class,'login']);

    //////
    ///
    /// Backoffice secured routes
    ///
    Route::group(['middleware'=>'auth:sanctum'],static function(){
        Route::get('/authUser',[AuthorizationController::class,'getAuthUser']);
        Route::post('/logout',[AuthorizationController::class,'logout']);

        Route::group(['prefix' => 'dashboard'],static function(){
            Route::get('/all-links',[DashboardController::class,'getCountAllLinks']);
            Route::get('/all-customers',[DashboardController::class,'getCountAllCustomers']);
        });

        Route::group(['prefix' => 'users'], static function(){
            Route::get('/',[UserController::class,'getUsers']);
            Route::get('/roles',[UserController::class,'getAllRoles']);
            Route::post('/',[UserController::class,'addNewUser']);
            Route::put('/',[UserController::class, 'editUser']);
            Route::delete('/{user}',[UserController::class,'deleteUser']);
            Route::get('/customers',[UserController::class,'getCustomers']);
        });

        Route::group(['prefix' => 'links'],static function(){
            Route::get('/types',[LinkController::class,'getLinkTypes']);
            Route::get('/',[LinkController::class,'getLinks']);
            Route::post('/',[LinkController::class,'addNewLink']);
            Route::put('/',[LinkController::class, 'editLink']);
            Route::delete('/{link}',[LinkController::class,'deleteLink']);
        });

        Route::group(['prefix' => 'tracking'],static function(){
            Route::get('/links',[TrackingController::class,'getTrackingByLink']);
            Route::post('/',[TrackingController::class,'addTracking']);
            Route::get('/{user}',[TrackingController::class,'getUserTracking']);
        });

    });
});

