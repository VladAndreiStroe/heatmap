<?php

namespace Database\Seeders;

use App\General\Concrete\Enums\Types\UserTypes;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456'),
            'type' => UserTypes::BACKOFFICE_USER_ID
        ]);

        $user->save();

        $customer = new User([
            'name' => 'Customer 1',
            'email' => 'customer1@mail.com',
            'password' => '123456',
            'type' => UserTypes::CUSTOMER_ID
        ]);

        $customer->save();

        $customer = new User([
            'name' => 'Customer 2',
            'email' => 'customer2@mail.com',
            'password' => '123456',
            'type' => UserTypes::CUSTOMER_ID
        ]);

        $customer->save();
    }
}
