import api from '../config';

const prefix = '/dashboard';

export const getCountAllLinks = () => api.get(prefix + '/all-links');

export const getCountAllCustomers = () => api.get(prefix + '/all-customers');
