import api from '../config';

const prefix = '/users';

export const getUser = () => api.get('/test');

export const getAuthenticatedUser = () => api.get('/authUser');

export const getAllUsers = () => api.get(prefix);

export const getAllRoles = () => api.get(prefix+'/roles');

export const addNewUser = ({name,email,role,password}) => api.post(prefix,{name:name,email:email,role:role,password:password});

export const editUser = ({id,name,email,role,password}) => api.put(prefix,{id: id, name:name,email:email,role:role,password:password});

export const deleteUser = (id) => api.delete(prefix+"/"+id);

export const getAllCustomers = () => api.get(prefix + '/' + 'customers')
