import api from '../config';

export const loginUser = ({email,password}) => api.post('/login',{
    email:email,
    password:password
});

export const logoutUser = () => api.post('/logout');
