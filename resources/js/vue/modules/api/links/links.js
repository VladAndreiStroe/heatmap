import api from '../config';

const prefix = '/links';

export const getAllLinks = () => api.get(prefix);

export const getLinkTypes = () => api.get(prefix + '/types');

export const addNewLink = ({address,type}) => api.post(prefix,{
    address:address,
    type:type
});

export const editLink = ({id,address,type}) => api.put(prefix,{
    id:id,
    address:address,
    type:type
});

export const deleteLink = (id) => api.delete(prefix+"/"+id);
