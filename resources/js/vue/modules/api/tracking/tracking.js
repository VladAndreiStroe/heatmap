import api from '../config';

const prefix = '/tracking';

export const addTracking = ({link_id,link_parameters,accessed_at,user_id}) => api.post(prefix,{
    link_id:link_id,
    link_parameters:link_parameters,
    accessed_at:accessed_at,
    user_id:user_id
});

export const getUserTracking = (id) => api.get(prefix + '/' + id);

export const getTrackingByLinks = () => api.get(prefix + '/links');

export const getTrackingByLinksWithTime = ({from,to}) => api.get(prefix + '/links',{params:{from: from,to:to}});
