import axios from 'axios';
import store from "../../store";

const instance =  axios.create({
    baseURL: '/api/backoffice/'
});

instance.interceptors.request.use(
    function(config) {
        if(!store.state.isLoading){
            store.dispatch('loading', true);
        }
        const token = localStorage.getItem("token");
        if (token) {
            config.headers["Authorization"] = 'Bearer ' + token;
        }
        return config;
    },
    function(error) {
        return Promise.reject(error);
    }
);

instance.interceptors.response.use(
    function(config){
        if(store.state.isLoading){
            store.dispatch('loading', false);
        }
        return config;
    },
    function (error){
        return Promise.reject(error);
    }
);

export default instance;
