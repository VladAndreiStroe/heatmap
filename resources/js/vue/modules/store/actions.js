export const actions = {
    login (context,{token,user}){
        if(token){
            localStorage.setItem('token',token);
        }
        context.commit('login',{
            user:{
                email:user.email,
                avatar:user.avatar,
                name:user.name,
                id:user.id,
                type:user.type
            }
        });
    },
    logout (context){
        if(localStorage.getItem('token')){
            localStorage.removeItem('token');
        }
        context.commit('logout');
    },
    loading (context,isLoading){
        if(isLoading)
            context.commit('loading');
        else
            context.commit('stopLoading');
    },
    toggleSidebar(context,toggle){
        if(toggle){
            context.commit('openSidebar');
        }
        else
        {
            context.commit('closeSidebar');
        }
    }
};
