export const getters = {
    user: state => {
        return state.user;
    },
    isAuthenticated: state => {
        return state.isAuthenticated;
    },
    isLoading: state => {
        return state.isLoading;
    },
    sidebar: state =>{
        return state.sidebar;
    }
};
