export const mutations = {
    login: (state,{user}) => {
        state.isAuthenticated = true;
        state.user = user;
    },
    logout: state => {
        state.isAuthenticated = false;
    },
    loading: state => {
        state.isLoading = true;
    },
    stopLoading: state => {
        state.isLoading = false;
    },
    openSidebar: state => {
        state.sidebar = true;
    },
    closeSidebar: state => {
        state.sidebar = false;
    }
};
