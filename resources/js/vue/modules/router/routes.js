import Login from "../../views/Login";
import Dashboard from "../../views/Dashboard";
import Users from "../../views/Users";
import Links from "../../views/Links";
import MockData from "../../views/MockData";
import Tracking from "../../views/Tracking";
import LinksTracking from "../../views/admin/tracking/LinksTracking";
import Customers from "../../views/admin/tracking/Customers";

export const routes = [
    {
        name:'Login',
        component:Login,
        path:"/backoffice/login",
        meta:{
            requiresAuth:false
        }
    },
    {
        name: 'Dashboard',
        component: Dashboard,
        path: '/backoffice/dashboard',
        meta:{
            requiresAuth: true
        }
    },
    {
        name: 'Tracking',
        component: Tracking,
        path: '/backoffice/tracking',
        meta:{
            requiresAuth: true
        },
        children:[
            {
                name:"LinksTracking",
                path:'links',
                component:LinksTracking,
                meta:{
                    requiresAuth: true
                }
            },
            {
                name: "Customers",
                path:'customers',
                component: Customers,
                meta:{
                    requiresAuth: true
                }
            }
        ]
    },
    {
        name: 'Users',
        component: Users,
        path: '/backoffice/users',
        meta:{
            requiresAuth: true
        }
    },
    {
        name: 'Links',
        component: Links,
        path: '/backoffice/links',
        meta:{
            requiresAuth: true
        }
    },
    {
        name:'Mock',
        component: MockData,
        path: '/backoffice/mock',
        meta:{
            requiresAuth: true
        }
    }
];
