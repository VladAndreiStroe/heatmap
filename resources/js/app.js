require('./bootstrap');

import Vue from "vue";
import Vuex from "vuex";
import VueRouter from "vue-router";

import Buefy from "buefy";

import App from "./vue/App";

import routes from './vue/modules/router';
import store from './vue/modules/store';

const router = new VueRouter({
    routes,
    mode: 'history'
})

router.beforeEach((to, from, next) => {

    store.dispatch('toggleSidebar',false);

    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!store.getters.isAuthenticated) {
            next({
                path: '/backoffice/login',
                query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})

Vue.use(Buefy);
Vue.use(Vuex);
Vue.use(VueRouter);

const app = new Vue({
    el:'#app',
    router,
    store,
    render: h => h(App)
});
