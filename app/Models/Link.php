<?php

namespace App\Models;

use App\General\Traits\HasType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    use HasFactory, HasType;

    protected $fillable = [
        'address',
        'type'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function tracking()
    {
        return $this->hasMany(Tracking::class,'link_id','id');
    }
}
