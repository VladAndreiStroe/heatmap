<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    use HasFactory;

    protected $fillable = [
        'link_id',
        'link_parameters',
        'accessed_at',
        'user_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'accessed_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function link()
    {
        return $this->belongsTo(Link::class);
    }
}
