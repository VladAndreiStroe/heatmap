<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;

    protected $fillable = [
        'type_id',
        'typeable_id',
        'typeable_type'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function typeable(){
        return $this->morphTo();
    }
}
