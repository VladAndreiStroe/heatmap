<?php


namespace App\General\Interfaces;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface IRepository
{
    public function store(array $args): ?Model;

    /**
     * Get all entries
     * @return Collection
     */
    public function getAll():Collection;

    /**
     * Get entry by providing the id
     *
     * @param int $id
     *
     * @return Model
     */
    public function getById(int $id): Model;

    public function update(Model $model,array $args): ?Model;
    public function delete(Model $model): ?Model;

}
