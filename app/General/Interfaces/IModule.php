<?php


namespace App\General\Interfaces;


interface IModule
{
    public function getRepository():IRepository;
}
