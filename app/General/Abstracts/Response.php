<?php


namespace App\General\Abstracts;

use Illuminate\Http\Response as LaravelResponse;

abstract class Response extends LaravelResponse
{
    protected int $responseCode;
    protected string $responseType;
    protected array $responseContent;

    public function __construct(array $responseContent, array $headers = [])
    {
        $this->responseContent = $responseContent;

        parent::__construct($this->responseContent, $this->responseCode, $headers);
    }


}
