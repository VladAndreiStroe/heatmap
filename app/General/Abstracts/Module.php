<?php


namespace App\General\Abstracts;


use App\General\Interfaces\IModule;
use App\General\Interfaces\IRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class Module
 *
 * @package App\General\Abstracts
 */
abstract class Module implements IModule
{
    protected IRepository $repository;
    protected string $resource;
    protected string $resourceCollection;

    public function __construct(IRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getRepository(): IRepository
    {
        return $this->repository;
    }

    /**
     * Get the resource associated with the module
     * @param Model $model
     *
     * @return JsonResource
     */
    public function getResource(Model $model): JsonResource
    {
        return new $this->resource($model);
    }

    /**
     * Get the collection associated with the module's resource
     * @param Collection $collection
     *
     * @return ResourceCollection
     */
    public function getResourceCollection(Collection $collection):ResourceCollection
    {
        return new $this->resourceCollection($collection);
    }
}
