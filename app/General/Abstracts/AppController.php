<?php


namespace App\General\Abstracts;


use App\General\Exceptions\ModuleNotFound;
use App\General\Interfaces\IAppController;
use App\General\Interfaces\IModule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

/**
 * Class AppController
 *
 * @package App\General\Abstracts
 */
abstract class AppController extends Controller implements IAppController
{
    private array $modules;

    public function __construct()
    {
        $this->setModules();

        if(!$this->validateModules())
        {
            throw new \Exception("A module is not a valid IModule implementation");
        }

        $this->setControllerProperties();
    }

    private function validateModules():bool
    {
        foreach ($this->modules as $module)
        {
            if(!($module instanceof IModule))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $module
     *
     * @return IModule
     * @throws ModuleNotFound
     */
    protected function getModule(string $module):IModule
    {
        foreach ($this->modules as $m)
        {
            if($module === get_class($m))
            {
                return $m;
            }
        }

        throw new ModuleNotFound("The module:". $module ." was not found");
    }

    private function setModules(): void
    {
        $class = new \ReflectionClass(static::class);

        $modules = config('appControllers.'.$class->getShortName().'.modules');

        foreach($modules as $key => $value){
            $this->modules[$key] = App::make($value);
        }
    }

    private function setControllerProperties():void
    {
        foreach ($this->modules as $moduleName => $moduleClass)
        {
            $this->$moduleName = $moduleClass;
        }
    }
}
