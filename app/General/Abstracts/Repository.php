<?php


namespace App\General\Abstracts;

use App\General\Interfaces\IRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

abstract class Repository implements IRepository
{
    /**
     * Model that the repository should use
     *
     * @var Model $model
     */
    protected Model $model;

    public function store(array $args): ?Model
    {
        $model = new $this->model();

        $model->fill($args);

        if($model->save()){
            return $model;
        }

        return null;
    }

    public function getAll(): Collection
    {
        return $this->model::all();
    }

    public function getCountAll(string $column = null, string $value = null):int
    {
        if($column === null && $value === null) {
            return $this->model::count();
        }

        return $this->model::where($column, $value)->count();
    }

    public function getById(int $id): Model
    {
        return $this->model::find($id);
    }

    public function getByColumn($value, string $columnName): ?Model
    {
        return $this->model::where($columnName, $value)->first();
    }

    public function getAllByColumn($value, string $columnName): ?Collection
    {
        return $this->model::where($columnName,$value)->get();
    }

    public function update(Model $model,array $args): ?Model
    {
        if($model->update($args)){
            return $model;
        }

        return null;
    }

    public function delete(Model $model): ?Model
    {
        try {
            if ($model->delete()) {
                return $model;
            }
        } catch (\Exception $e) {
            Log::debug("Error deleting {$model->toJson()}. {$e->getMessage()}");
        }

        return null;
    }
}
