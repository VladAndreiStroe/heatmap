<?php


namespace App\General\Repositories;


use App\General\Abstracts\Repository;
use App\Models\Link;
use Illuminate\Database\Eloquent\Model;

class LinkRepository extends Repository
{

    public function __construct(Link $link)
    {
        $this->model = $link;
    }

}
