<?php


namespace App\General\Repositories;


use App\General\Abstracts\Repository;
use App\General\Concrete\Enums\Types\UserTypes;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserRepository
 * @package App\General\Repositories
 */
class UserRepository extends Repository
{
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function store(array $args): ?Model
    {
        $args['password'] = Hash::make($args['password']);

        /** @var User $user */
        $user = parent::store($args);

        if(!empty($args['role'])){
            $user->assignRole($args['role']);
        }

        return $user;
    }

    public function update(Model $model, array $args): ?Model
    {
        /** @var User $user */
        $user = parent::update($model, $args);

        if($user instanceof User){
            if(!$user->hasRole($args['role'])){
                $user->syncRoles($args['role']);
            }
            return $user;
        }

        return null;
    }

    public function getAllCustomers(): Collection
    {
        return $this->getAllByColumn(UserTypes::CUSTOMER_ID, 'type');
    }
}
