<?php


namespace App\General\Repositories;


use App\General\Abstracts\Repository;
use App\Models\Tracking;
use Illuminate\Support\Facades\DB;

class TrackingRepository extends Repository
{
    public function __construct(Tracking $tracking)
    {
        $this->model = $tracking;
    }

    public function getTrackingByLink($from = null,$to = null)
    {
        if($from === null && $to === null)
        {
            return $this->model::select('link_id',DB::raw('count(link_id) clicks'))->with(['link' => static function ($query){
                $query->select('id','address','type');
            }])
                ->orderBy('link_id','asc')
                ->groupBy('link_id')
                ->get();
        }

        return $this->model::select('link_id',DB::raw('count(link_id) clicks'))->with(['link' => static function ($query){
            $query->select('id','address','type');
        }])
            ->whereBetween('accessed_at',[$from,$to])
            ->orderBy('link_id','asc')
            ->groupBy('link_id')
            ->get();

    }

    public function getLinksByCliks()
    {

    }
}
