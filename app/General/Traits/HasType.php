<?php


namespace App\General\Traits;


use App\Models\Type;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * Trait HasType
 *
 * @package App\General\Traits
 */
trait HasType
{
    /**
     * @return MorphOne
     */
    public function type(): MorphOne
    {
        return $this->morphOne(Type::class,'typeable');
    }
}
