<?php


namespace App\General\Traits;


trait IsSingleton
{
    private static ?object $instance = null;

    protected function __construct()
    {

    }

    /**
     * @return object
     */
    public static function GetInstance():object
    {
        if(static::$instance === null)
        {
            static::$instance = new static();
        }

        return static::$instance;
    }
}
