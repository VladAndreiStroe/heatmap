<?php


namespace App\General\Concrete\Enums\Types;


use App\General\Abstracts\Enum;

class ResponseTypes extends Enum
{
    public const SUCCESS = 'success';
    public const NOT_FOUND = 'notFound';
    public const SERVER_ERROR = 'serverError';
    public const UNAUTHORIZED = 'unauthorized';
    public const BAD_REQUEST = 'badRequest';

    public const SUCCESS_CODE = 200;
    public const NOT_FOUND_CODE = 404;
    public const SERVER_ERROR_CODE = 500;
    public const UNAUTHORIZED_CODE = 401;
    public const BAD_REQUEST_CODE = 400;

    protected static array $enum = [
        self::SUCCESS => self::SUCCESS_CODE,
        self::NOT_FOUND => self::NOT_FOUND_CODE,
        self::SERVER_ERROR => self::SERVER_ERROR_CODE,
        self::UNAUTHORIZED => self::UNAUTHORIZED_CODE,
        self::BAD_REQUEST => self::BAD_REQUEST_CODE
    ];
}
