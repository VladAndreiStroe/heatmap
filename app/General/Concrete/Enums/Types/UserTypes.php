<?php


namespace App\General\Concrete\Enums\Types;


use App\General\Abstracts\Enum;

class UserTypes extends Enum
{
    public const CUSTOMER = 'customer';
    public const BACKOFFICE_USER = 'backoffice';

    public const BACKOFFICE_USER_ID = 0;
    public const CUSTOMER_ID = 1;

    protected static array $enum = [
        self::BACKOFFICE_USER => self::BACKOFFICE_USER_ID,
        self::CUSTOMER => self::CUSTOMER_ID
    ];
}
