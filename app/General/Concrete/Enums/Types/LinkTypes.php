<?php


namespace App\General\Concrete\Enums\Types;


use App\General\Abstracts\Enum;

class LinkTypes extends Enum
{
    protected const PRODUCT = 'product';
    protected const CATEGORY = 'category';
    protected const STATIC_PAGE = 'static-page';
    protected const CHECKOUT = 'checkout';
    protected const HOMEPAGE = 'homepage';

    protected const PRODUCT_ID = 0;
    protected const CATEGORY_ID = 1;
    protected const STATIC_PAGE_ID = 2;
    protected const CHECKOUT_ID = 3;
    protected const HOMEPAGE_ID = 4;

    protected static array $enum = [
        self::PRODUCT => self::PRODUCT_ID,
        self::CATEGORY => self::CATEGORY_ID,
        self::STATIC_PAGE => self::STATIC_PAGE_ID,
        self::CHECKOUT => self::CHECKOUT_ID,
        self::HOMEPAGE => self::HOMEPAGE_ID
    ];
}
