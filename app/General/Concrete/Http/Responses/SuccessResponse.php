<?php


namespace App\General\Concrete\Http\Responses;


use App\General\Abstracts\Response;
use App\General\Concrete\Enums\Types\ResponseTypes;

class SuccessResponse extends Response
{
    protected int $responseCode = ResponseTypes::SUCCESS_CODE;
}
