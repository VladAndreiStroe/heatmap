<?php


namespace App\General\Concrete\Http\Responses;


use App\General\Abstracts\Response;
use App\General\Concrete\Enums\Types\ResponseTypes;

class UnauthorizedRequestResponse extends Response
{
    protected int $responseCode = ResponseTypes::UNAUTHORIZED_CODE;
}
