<?php


namespace App\General\Concrete\Http\Responses;


use App\General\Abstracts\Response;
use App\General\Concrete\Enums\Types\ResponseTypes;

class NotFoundResponse extends Response
{
    protected int $responseCode = ResponseTypes::NOT_FOUND_CODE;
}
