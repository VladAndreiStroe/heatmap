<?php


namespace App\General\Concrete\Http\Responses;


use App\General\Abstracts\Response;
use App\General\Concrete\Enums\Types\ResponseTypes;

class BadRequestResponse extends Response
{
    protected int $responseCode = ResponseTypes::BAD_REQUEST_CODE;
}
