<?php


namespace App\General\Concrete\Modules;


use App\General\Abstracts\Module;
use App\General\Repositories\UserRepository;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserCollection;

class UserModule extends Module
{
    protected string $resource = UserResource::class;
    protected string $resourceCollection = UserCollection::class;

    /**
     * UserModule constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        parent::__construct($repository);
    }
}
