<?php


namespace App\General\Concrete\Modules;


use App\General\Abstracts\Module;
use App\General\Repositories\TrackingRepository;
use App\Http\Resources\Tracking as TrackingResource;
use App\Http\Resources\TrackingCollection;

class TrackingModule extends Module
{
    protected string $resource = TrackingResource::class;
    protected string $resourceCollection = TrackingCollection::class;

    /**
     * TrackingModule constructor.
     *
     * @param TrackingRepository $repository
     */
    public function __construct(TrackingRepository $repository)
    {
        parent::__construct($repository);
    }

}
