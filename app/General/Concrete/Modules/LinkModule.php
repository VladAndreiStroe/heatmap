<?php


namespace App\General\Concrete\Modules;


use App\General\Abstracts\Module;
use App\General\Repositories\LinkRepository;
use App\Http\Resources\Link as LinkResource;
use App\Http\Resources\LinkCollection;

class LinkModule extends Module
{
    protected string $resource = LinkResource::class;
    protected string $resourceCollection = LinkCollection::class;

    /**
     * LinkModule constructor.
     *
     * @param LinkRepository $repository
     */
    public function __construct(LinkRepository $repository)
    {
        parent::__construct($repository);
    }
}
