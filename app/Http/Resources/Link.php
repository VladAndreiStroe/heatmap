<?php

namespace App\Http\Resources;

use App\General\Concrete\Enums\Types\LinkTypes;
use Illuminate\Http\Resources\Json\JsonResource;

class Link extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'address' => $this->address,
            'type' => [
                'name' => LinkTypes::getValueById($this->type),
                'id' => $this->type
            ]
        ];
    }
}
