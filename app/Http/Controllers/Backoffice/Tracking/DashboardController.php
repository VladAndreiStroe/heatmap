<?php


namespace App\Http\Controllers\Backoffice\Tracking;

use App\General\Abstracts\AppController;
use App\General\Abstracts\Response;
use App\General\Concrete\Enums\Types\UserTypes;
use App\General\Concrete\Http\Responses\SuccessResponse;
use App\General\Concrete\Modules\LinkModule;
use App\General\Concrete\Modules\TrackingModule;
use App\General\Concrete\Modules\UserModule;

/**
 * Class DashboardController
 * @property LinkModule $linkModule
 * @property UserModule $userModule
 * @property TrackingModule $trackingModule
 *
 * @package App\Http\Controllers\Backoffice\Tracking
 */
class DashboardController extends AppController
{
    public function getCountAllLinks():Response
    {
        return new SuccessResponse(
            ['count' => $this->linkModule->getRepository()->getCountAll()]
        );
    }

    public function getCountAllCustomers():Response
    {
        return new SuccessResponse([
            'count' => $this->userModule->getRepository()->getCountAll('type', UserTypes::CUSTOMER_ID)
        ]);
    }
}
