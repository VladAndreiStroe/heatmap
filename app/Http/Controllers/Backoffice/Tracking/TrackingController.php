<?php

namespace App\Http\Controllers\Backoffice\Tracking;

use App\General\Abstracts\AppController;
use App\General\Concrete\Http\Responses\BadRequestResponse;
use App\General\Concrete\Http\Responses\SuccessResponse;
use App\General\Concrete\Modules\LinkModule;
use App\General\Concrete\Modules\TrackingModule;
use App\General\Concrete\Modules\UserModule;
use App\Http\Requests\Backoffice\Tracking\AddNewTrackingRequest;
use App\Http\Resources\TrackingLinkCollection;
use App\Models\Tracking;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class TrackingController
 *
 * @property TrackingModule $trackingModule
 * @property UserModule $userModule
 * @package App\Http\Controllers\Backoffice\Tracking
 */
class TrackingController extends AppController
{
    public function addTracking(AddNewTrackingRequest $addNewTrackingRequest): Response
    {
        if($addNewTrackingRequest->validated()){
            $tracking =  $this->trackingModule->getRepository()->store($addNewTrackingRequest->all());

            if($tracking instanceof Tracking){
                return new SuccessResponse([
                    'tracking' => $this->trackingModule->getResource($tracking)
                ]);
            }
        }

        return new BadRequestResponse([
            'fail' => true
        ]);
    }

    public function getUserTracking(User $user): Response
    {
        if($user instanceof User)
        {
            return new SuccessResponse([
                'tracking' => $this->trackingModule->getResourceCollection($user->tracking)
            ]);
        }

        return new BadRequestResponse([
            'message' => 'Invalid user'
        ]);
    }

    public function getTrackingByLink(Request $request): Response
    {
        $from = $request->get('from');
        $to = $request->get('to');

        return new SuccessResponse([
            'tracking' => new TrackingLinkCollection($this->trackingModule->getRepository()->getTrackingByLink($from,$to))
        ]);
    }
}
