<?php

namespace App\Http\Controllers\Backoffice\Authorization;

use App\General\Abstracts\AppController;
use App\General\Concrete\Enums\Types\UserTypes;
use App\General\Concrete\Http\Responses\BadRequestResponse;
use App\General\Concrete\Http\Responses\SuccessResponse;
use App\General\Concrete\Http\Responses\UnauthorizedRequestResponse;
use App\General\Concrete\Modules\UserModule;
use App\Http\Requests\Backoffice\Authorization\LoginRequest;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\User as UserResource;

/**
 * Class AuthorizationController
 *
 * @property UserModule userModule
 * @package App\Http\Controllers\Backoffice\Authorization
 */
class AuthorizationController extends AppController
{

    public function login(LoginRequest $loginRequest):Response
    {
        if($loginRequest->validated()){
            /** @var User $user */
            $user = $this->userModule->getRepository()->getByColumn($loginRequest->post('email'),'email');

            if($user->type === UserTypes::BACKOFFICE_USER_ID){
                if(Hash::check($loginRequest->post('password'),$user->password)){
                    $token = $user->createToken('backofficeToken');
                    return new SuccessResponse([
                        'user' => new UserResource($user),
                        'password' => true,
                        'token' => $token->plainTextToken
                    ]);
                }
            }
            else{
                return new UnauthorizedRequestResponse([
                    'unauthorized' => true
                ]);
            }

            return new BadRequestResponse([
                'user' => $user,
                'password' => false
            ]);
        }
        return new BadRequestResponse(['test' => false]);
    }

    /**
     * @return UserResource|null
     */
    public function getAuthUser(): ?Response
    {
        $user = Auth::user();

        return $user ? new SuccessResponse([
            'user' =>  new UserResource($user)
        ]): null;
    }

    /**
     * Logout function
     */
    public function logout():Response
    {
        /** @var User $user */
        $user = Auth::user();

        if($user->tokens()->where('id', $user->currentAccessToken()->id)->delete())
        {
            return new SuccessResponse(['success'=> true]);
        }

        return new BadRequestResponse([
            'success' => false
        ]);
    }
}
