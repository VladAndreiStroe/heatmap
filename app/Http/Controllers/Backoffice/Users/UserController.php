<?php


namespace App\Http\Controllers\Backoffice\Users;

use App\General\Abstracts\AppController;
use App\General\Abstracts\Response;
use App\General\Concrete\Http\Responses\BadRequestResponse;
use App\General\Concrete\Http\Responses\SuccessResponse;
use App\General\Concrete\Modules\UserModule;
use App\Http\Requests\Backoffice\User\AddNewLinkRequest;
use App\Http\Requests\Backoffice\User\AddNewUserRequest;
use App\Http\Requests\Backoffice\User\EditLinkRequest;
use App\Http\Requests\Backoffice\User\EditUserRequest;
use App\Http\Resources\RoleCollection;
use App\Models\User;
use Spatie\Permission\Models\Role;

/**
 * Class UserController
 *
 * @property UserModule $userModule
 * @package App\Http\Controllers\Backoffice\Users
 */
class UserController extends AppController
{
    public function getUsers(): Response
    {
        return new SuccessResponse(
            [
                'users' => $this->userModule->getResourceCollection($this->userModule->getRepository()->getAll())
            ]
        );
    }

    public function getAllRoles():Response
    {
        return new SuccessResponse([
            'roles' => new RoleCollection(Role::all())
        ]);
    }

    public function addNewUser(AddNewUserRequest $addNewUserRequest):Response
    {
        if($addNewUserRequest->validated()){
            $user =  $this->userModule->getRepository()->store($addNewUserRequest->all());

            if($user instanceof User){
                return new SuccessResponse([
                    'user' => $this->userModule->getResource($user)
                ]);
            }
        }

        return new BadRequestResponse([
            'fail' => true
        ]);
    }

    public function editUser(EditUserRequest $editUserRequest):Response
    {
        if($editUserRequest->validated()){

            $user = $this->userModule->getRepository()->getById($editUserRequest->input('id'));

            $args = $editUserRequest->all(
                [
                    'name','email','role','password'
                ]
            );

            $args = array_filter($args,static function($value){
                return $value !== null;
            });

            $user = $this->userModule->getRepository()->update($user,$args);

            if($user instanceof User){
                return new SuccessResponse([
                    'user' => $this->userModule->getResource($user)
                ]);
            }
        }

        return new BadRequestResponse([
            'fail' => true
        ]);
    }

    public function deleteUser(User $user):Response
    {
        if($user instanceof User){

            $deletedUser = $this->userModule->getRepository()->delete($user);

            if($deletedUser instanceof User){
                return new SuccessResponse([
                    'user' => $this->userModule->getResource($user)
                ]);
            }
            return new BadRequestResponse([
                'fail' => true
            ]);

        }
        return new BadRequestResponse([
            'fail' => true
        ]);
    }

    public function getCustomers(): Response
    {
        return new SuccessResponse([
            'customers' => $this->userModule->getRepository()->getAllCustomers()
        ]);
    }
}
