<?php

namespace App\Http\Controllers\Backoffice\Links;

use App\General\Abstracts\AppController;
use App\General\Concrete\Enums\Types\LinkTypes;
use App\General\Concrete\Http\Responses\BadRequestResponse;
use App\General\Concrete\Http\Responses\SuccessResponse;
use App\General\Concrete\Modules\LinkModule;
use App\Http\Requests\Backoffice\Links\AddNewLinkRequest;
use App\Http\Requests\Backoffice\Links\AddNewTrackingRequest;
use App\Http\Requests\Backoffice\Links\EditLinkRequest;
use App\Models\Link;
use Illuminate\Http\Response;

/**
 * Class UserController
 *
 * @property LinkModule $linkModule
 * @package App\Http\Controllers\Backoffice\Links
 */
class LinkController extends AppController
{
    public function getLinks(): Response
    {
        return new SuccessResponse(
            [
                'links' => $this->linkModule->getResourceCollection($this->linkModule->getRepository()->getAll())
            ]
        );
    }

    public function getLinkTypes():Response
    {
        return new SuccessResponse([
            'types' => LinkTypes::getEnum()
        ]);
    }

    public function addNewLink(AddNewLinkRequest $addNewLinkRequest):Response
    {
        if($addNewLinkRequest->validated()){
            $link =  $this->linkModule->getRepository()->store($addNewLinkRequest->all());

            if($link instanceof Link){
                return new SuccessResponse([
                    'link' => $this->linkModule->getResource($link)
                ]);
            }
        }

        return new BadRequestResponse([
            'fail' => true
        ]);
    }

    public function editLink(EditLinkRequest $editLinkRequest):Response
    {
        if($editLinkRequest->validated()){

            $link = $this->linkModule->getRepository()->getById($editLinkRequest->input('id'));

            $args = $editLinkRequest->all(
                [
                    'address'
                ]
            );

            $args = array_filter($args,static function($value){
                return $value !== null;
            });

            $link = $this->linkModule->getRepository()->update($link,$args);

            if($link instanceof Link){
                return new SuccessResponse([
                    'link' => $this->linkModule->getResource($link)
                ]);
            }
        }

        return new BadRequestResponse([
            'fail' => true
        ]);
    }

    public function deleteLink(Link $link):Response
    {
        if($link instanceof Link){

            $deletedLink = $this->linkModule->getRepository()->delete($link);

            if($deletedLink instanceof Link){
                return new SuccessResponse([
                    'link' => $this->linkModule->getResource($link)
                ]);
            }
            return new BadRequestResponse([
                'fail' => true
            ]);

        }
        return new BadRequestResponse([
            'fail' => true
        ]);
    }
}
