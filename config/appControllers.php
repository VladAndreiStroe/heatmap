<?php

use App\General\Concrete\Modules\LinkModule;
use App\General\Concrete\Modules\TrackingModule;
use App\General\Concrete\Modules\UserModule;

/*
 * In order to set modules, the first property should be the name of the actual AppController
 * and in the second array named modules should be added the modules with their names as the name of the property and
 * the values as the Class::class
 */
return [
    'AuthorizationController' => [
        'modules' => [
            'userModule' => UserModule::class
        ]
    ],
    'UserController' => [
        'modules' => [
            'userModule' => UserModule::class
        ]
    ],
    'LinkController' => [
        'modules' => [
            'linkModule' => LinkModule::class,
        ]
    ],
    'TrackingController' => [
        'modules' => [
            'trackingModule' => TrackingModule::class,
            'userModule' => UserModule::class,
            'linkModule' => LinkModule::class
        ]
    ],
    'DashboardController' => [
        'modules' => [
            'trackingModule' => TrackingModule::class,
            'userModule' => UserModule::class,
            'linkModule' => LinkModule::class
        ]
    ]
];
