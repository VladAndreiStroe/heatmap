## Steps to run project

- Create .env file in root directory with the following vars:
    - APP_NAME=Laravel
    - APP_ENV=local 
    - APP_KEY=base64:vO3vivCY3oQK3icz6ONTW0VxPS25zbOYRJsakM1imNs=
    - APP_DEBUG=true
    - APP_URL=http://heatmap.test
    - SAIL_DEBUG=true

    - LOG_CHANNEL=stack
    - LOG_LEVEL=debug

    - DB_CONNECTION=mysql
    - DB_HOST=mysql
    - DB_PORT=3306
    - DB_DATABASE=heatmap
    - DB_USERNAME=sail
    - DB_PASSWORD=password

    - BROADCAST_DRIVER=log
    - CACHE_DRIVER=file
    - FILESYSTEM_DRIVER=local
    - QUEUE_CONNECTION=sync
    - SESSION_DRIVER=file
    - SESSION_LIFETIME=120

    - MEMCACHED_HOST=memcached

    - REDIS_HOST=redis
    - REDIS_PASSWORD=null
    - REDIS_PORT=6379

    - MAIL_MAILER=smtp
    - MAIL_HOST=mailhog
    - MAIL_PORT=1025
    - MAIL_USERNAME=null
    - MAIL_PASSWORD=null
    - MAIL_ENCRYPTION=null
    - MAIL_FROM_ADDRESS=null
    - MAIL_FROM_NAME="${APP_NAME}"

    - AWS_ACCESS_KEY_ID=
    - AWS_SECRET_ACCESS_KEY=
    - AWS_DEFAULT_REGION=us-east-1
    - AWS_BUCKET=
    - AWS_USE_PATH_STYLE_ENDPOINT=false

    - PUSHER_APP_ID=
    - PUSHER_APP_KEY=
    - PUSHER_APP_SECRET=
    - PUSHER_APP_CLUSTER=mt1

    - MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
    - MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

    - SCOUT_DRIVER=meilisearch
    - MEILISEARCH_HOST=http://meilisearch:7700
    

- Commands to run: 
    - composer install
    - ./vendor/bin/sail up (to run the docker containers)
    - npm install
    - npm run dev
    - ./vendor/bin/sail php artisan migrate:fresh --seed (db migration and seeders)
    

- Links:
    - localhost/backoffice/login 
        - username: admin@admin.com
        - password: 123456
    
- Important info: 
    - Some table rows are interactable, click on them for more functionality
    - In order to add mock data:
        - Go to mock page and add entries for every link that the customer access
